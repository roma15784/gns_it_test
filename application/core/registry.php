<?php

namespace Application\Core;

class Registry
{
	private static $_map;

	public static function set($key, $value)
	{
		if (!isset(self::$_map[$key])) {
			self::$_map[$key] = $value;
		}
	}

	public static function get($key)
	{
		if (isset(self::$_map[$key])) {
			return self::$_map[$key];
		} else {
			return null;
		}
	}

	public static function delete($key) {
		if (isset(self::$_map[$key])) {
			unset(self::$_map[$key]);
		}
	}
}
<?php

define('ROOT_DIR',$_SERVER['DOCUMENT_ROOT']);


spl_autoload_register(function($class)
{
	$classMap = array(
		'Application\Core\Router' => ROOT_DIR . '/application/core/router.php',
		'Application\Core\Registry' => ROOT_DIR . '/application/core/registry.php',
		'Application\Core\Controller' => ROOT_DIR . '/application/core/Controller.php',
		'Application\Controller\IndexController' => ROOT_DIR . '/application/controller/IndexController.php',
		'Application\Core\View' => ROOT_DIR . '/application/core/View.php',
		'Application\Model\Film' => ROOT_DIR . '/application/model/Film.php',
		'Application\Model\Fact' => ROOT_DIR . '/application/model/Fact.php',
		'Application\Controller\FilmController' => ROOT_DIR . '/application/controller/FilmController.php',
	);

	try {
		$isFound = false;
		foreach($classMap as $key => $value) {
			if ($class == $key) {
				$isFound = true;
				require_once($value);
			}
		}

		if (!$isFound) {
			throw new \Exception('Class ' . $class . ' was not loaded. File was not found. Check auto load function');
		}

	} catch (\Exception $e) { 
		echo $e->getMessage();
	}
});

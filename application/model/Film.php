<?php

namespace Application\Model;

use Application\Core\Registry;

class Film
{
	public function showActiveFilms()
	{
		$query = "SELECT * FROM `films` WHERE isActive = :isActive";
		$request = Registry::get('PDO')->prepare($query);
		$request->execute(array('isActive' => 'true'));
		return $request->fetchAll();
	}

	public function save($data)
	{
		$query = "INSERT INTO `films`(name, year, isActive) VALUES(:name, :year, :isActive)";
		$request = Registry::get('PDO')->prepare($query);
		$request->execute(array(
			'name' => $data['name'],
			'year' => $data['year'],
			'isActive' => $data['isActive'],
		));
	}
}
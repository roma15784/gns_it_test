<?php

namespace Application\Model;

use Application\Core\Registry;

class Fact
{
	public function selectByFilmId($id) {
		$query = "SELECT `f`.id, `f`.text
		FROM `facts` `f` JOIN `films` `fm` ON `f`.film_id = `fm`.id
		WHERE `fm`.id = :id";
		$request = Registry::get('PDO')->prepare($query);
		$request->execute(array('id' => $id));
		return $request->fetchAll();
	}

	public function save($data)
	{
		$query = "INSERT INTO `facts` (`text`, `film_id`) VALUES(:text, :film_id)";
		$request = Registry::get('PDO')->prepare($query);
		$request->execute(array('text' => $data['comment'], 'film_id' => $data['film_id']));
	}
}
CREATE DATABASE "gns_it";
USE "gns_it";

CREATE TABLE `films` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` TEXT NOT NULL,
	`year` INT NOT NULL,
	`isActive` ENUM('True','False') NOT NULL DEFAULT 'True',
	PRIMARY KEY (`id`)
);

CREATE TABLE `facts` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`text` INT NOT NULL,
	`film_id` INT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK__films` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`)
);
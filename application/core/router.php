<?php

namespace Application\Core;

class Router
{
	private $routeMap = array();

	public function add($route) {
		array_push($this->routeMap, $route);
	}

	public function run() {
		$url = explode('?', $_SERVER['REQUEST_URI']);
		$isRouteFound = false;
		$path = $url[0];
		try {
			foreach($this->routeMap as $route) {
				if ($route['path'] == $path) {
					$controllerClassName = "\\Application\\Controller\\" . $route['data']['controller'] . 'Controller';
					$action = $route['data']['action'] . 'Action';

					$controller = new $controllerClassName;

					if (method_exists($controller, $action)) {
						$isRouteFound = true;
						$controller->$action();
					} else {
						throw new \Exception('Method ' . $action . ' is not found in ' . $controllerClassName . ', check your configuration');
					}
				}
			}
			if (!$isRouteFound) throw new \Exception('Wrong route. Check your routing configuration');
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
}
<div class="container">
    <div class="starter-template">
        <h3>Films</h3>
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Year</th>
                <th>Action</th>
            </tr>
            <?php foreach($data['films'] as $film) { ?>
            <tr>
                <td><?=$film['id']?></td>
                <td><?=$film['name']?></td>
                <td><?=$film['year']?></td>
                <td><a href=<?php echo "/facts?id=" . $film['id'];?> class="btn btn-info btn-xs">Add fact</a></td>
            </tr>
            <?php } ?>
        </table>
        <a href="/films/new" class="btn btn-success">Add</a>
    </div>
</div>
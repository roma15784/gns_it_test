<div class="container">
    <div class="starter-template">
        <a href="/">back</a>
        <h4>Facts about this film</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <form action="/facts/save" method="GET">
                        <input type="hidden" name="film_id" value=<?=$_GET['id']?>> 
                        <label for="comment">Comment:</label><br>
                        <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Facts and commentaries:</b></div>
                    <div class="panel-body">
                        <p>There displayed some fact and commentaries about this film. You also can add yours.</p>
                    </div>
                    <ul class="list-group">
                        <?php foreach($data['facts'] as $fact) { ?>
                        <li class="list-group-item"><span class="label label-default">#<?=$fact['id']?></span> <?=$fact['text']?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="starter-template">
        <h3>Add film</h3>
        <div class="form-group">
            <form action="/films/save" method="GET">
                <table>
                    <tr>
                        <td><label for="name">Name:</label></td>
                        <td><input type="text" name="name" class="form-control" id="name"></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="year">Year:</label>
                        </td>
                        <td>
                            <select name="year" class="form-control" id="year">
                                <?php for ($i = 2015; $i > 1914; $i--) { ?>
                                <option value=<?=$i?>><?=$i?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="isActive">Active</label>
                        </td>
                        <td>
                            <select name="isActive" class="form-control" id="isActive">
                                <option value="true">True</option>
                                <option value="false">False</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>
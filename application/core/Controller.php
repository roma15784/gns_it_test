<?php

namespace Application\Core;

class Controller
{
	protected $template;

	public function __construct() {
		$this->template = new View('base');
	}

	public function _redirect($path)
	{
		header("Location: " . $path);
	}

	public function _getParam($key)
	{
		return !empty($_GET[$key]) ? $_GET[$key] : null;
	}
}
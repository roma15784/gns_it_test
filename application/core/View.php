<?php

namespace Application\Core;

class View
{
	private $baseTemplate;

	public function __construct($baseTemplate)
	{
		$this->baseTemplate = $baseTemplate;
		include_once ROOT_DIR . '/application/view/' . $this->baseTemplate . '.php';
	}
	
	public function render($template, $data = array())
	{
		include_once ROOT_DIR . '/application/view/' . $template . '.php';
	}
}
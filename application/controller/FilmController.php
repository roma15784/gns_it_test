<?php

namespace Application\Controller;

use Application\Core\Controller;
use Application\Model\Film;

class FilmController extends Controller
{
	public function saveAction()
	{
		$name = $this->_getParam('name');
		$year = $this->_getParam('year');
		$isActive = $this->_getParam('isActive');

		$film = new Film();
		$film->save(array('name' => $name, 'year' => $year, 'isActive' => $isActive));

		$this->_redirect('/');
	}

	public function addAction()
	{
		$this->template->render('Film/index');
	}
}
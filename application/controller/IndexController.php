<?php

namespace Application\Controller;

use Application\Core\Controller;
use Application\Model\Film;
use Application\Model\Fact;

class IndexController extends Controller
{
	public function indexAction() {
		$film = new Film();
		$films = $film->showActiveFilms();

		$this->template->render('Index/index', array('films' => $films));
	}

	public function addFactAction() {
		$id = $this->_getParam('id');

		$facts = array();
		if (!empty($id)) {
			$fact = new Fact();
			$facts = $fact->selectByFilmId($id);
		}

		$this->template->render('Index/facts', array('facts' => $facts));
	}

	public function saveFactAction() {
		$filmId = $this->_getParam('film_id');
		$factText = $this->_getParam('comment');

		$fact = new Fact();
		$fact->save(array('film_id' => $filmId, 'comment' => $factText));

		$this->_redirect('/facts?id=' . $filmId);
	}
}
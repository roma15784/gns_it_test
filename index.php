<?php

error_reporting(E_ALL);

require_once 'autoload.php';
require_once 'config.php';

use Application\Core\Router;
use Application\Core\Registry;

try {
	$pdoInstance = new \PDO("mysql:host=".HOST.";dbname=".DB, USER, PASS);
} catch (\Exception $e) {
	echo $e->getMessage();
}

Registry::set('PDO', $pdoInstance);

$router = new Router();

$router->add(array(
	'name' => 'home', 
	'path' => '/',
	'data' => array(
		'controller' => 'Index',
		'action' => 'index',
	),
));

$router->add(array(
	'name' => 'facts',
	'path' => '/facts',
	'data' => array(
		'controller' => 'Index',
		'action' => 'addFact',
	),
));

$router->add(array(
	'name' => 'save_facts',
	'path' => '/facts/save',
	'data' => array(
		'controller' => 'Index',
		'action' => 'saveFact',
	),
));

$router->add(array(
	'name' => 'save_films',
	'path' => '/films/save',
	'data' => array(
		'controller' => 'Film',
		'action' => 'save',
	),
));

$router->add(array(
	'name' => 'add_film',
	'path' => '/films/new',
	'data' => array(
		'controller' => 'Film',
		'action' => 'add',
	),
));

$router->run();

